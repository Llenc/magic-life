package com.example.magicvines.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.magicvines.R;
import com.google.android.material.snackbar.Snackbar;

public class MainFragment extends Fragment {

    private ImageButton lifetwotoone;
    private ImageButton lifeonetotwo;
    private Button p1poisonmore;
    private Button p2poisonmore;
    private Button p1poisonless;
    private Button p2poisonless;
    private ImageButton p1lifemore;
    private ImageButton p2lifemore;
    private ImageButton p1lifeless;
    private ImageButton p2lifeless;
    private TextView counter1;
    private TextView counter2;

    private int life1;
    private int life2;
    private int poison1;
    private int poison2;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        lifetwotoone = view.findViewById(R.id.lifetwotoone);
        lifeonetotwo = view.findViewById(R.id.lifeonetotwo);
        p1poisonmore = view.findViewById(R.id.p1poisonmore);
        p2poisonmore = view.findViewById(R.id.p2poisonmore);
        p1poisonless = view.findViewById(R.id.p1poisonless);
        p2poisonless = view.findViewById(R.id.p2poisonless);
        p1lifemore = view.findViewById(R.id.p1lifemore);
        p2lifemore = view.findViewById(R.id.p2lifemore);
        p1lifeless = view.findViewById(R.id.p1lifeless);
        p2lifeless = view.findViewById(R.id.p2lifeless);
        counter1 = view.findViewById(R.id.counter1);
        counter2 = view.findViewById(R.id.counter2);

        reset();

        if (savedInstanceState != null){
            life1 = savedInstanceState.getInt("life1");
            life2 = savedInstanceState.getInt("life2");
            poison1 = savedInstanceState.getInt("poison1");
            poison2 = savedInstanceState.getInt("poison2");
            updateViews();
        }

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.lifeonetotwo:
                        if (life1 > 0) {
                            life1--;
                            life2++;
                        }
                        break;
                    case R.id.lifetwotoone:
                        if (life2 > 0) {
                            life1++;
                            life2--;
                        }
                        break;
                    case R.id.p1lifemore:
                        life1++;
                        break;
                    case R.id.p2lifemore:
                        life2++;
                        break;
                    case R.id.p1lifeless:
                        if (life1 > 0) {
                            life1--;
                        }
                        break;
                    case R.id.p2lifeless:
                        if (life2 > 0) {
                            life2--;
                        }
                        break;
                    case R.id.p1poisonmore:
                        poison1++;
                        break;
                    case R.id.p2poisonmore:
                        poison2++;
                        break;
                    case R.id.p1poisonless:
                        if (poison1 > 0) {
                            poison1--;
                        }
                        break;
                    case R.id.p2poisonless:
                        if (poison2 > 0) {
                            poison2--;
                        }
                        break;
                }
                updateViews();
            }
        };



        lifeonetotwo.setOnClickListener(listener);
        lifetwotoone.setOnClickListener(listener);
        p1lifemore.setOnClickListener(listener);
        p2lifemore.setOnClickListener(listener);
        p1lifeless.setOnClickListener(listener);
        p2lifeless.setOnClickListener(listener);
        p1poisonmore.setOnClickListener(listener);
        p2poisonmore.setOnClickListener(listener);
        p1poisonless.setOnClickListener(listener);
        p2poisonless.setOnClickListener(listener);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_principal,menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==R.id.refresh){
            reset();
            Snackbar.make(getView(),"Reinicio de partida",Snackbar.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    private void reset(){
        poison1 = 0;
        poison2 = 0;
        life1 = 20;
        life2 = 20;

        updateViews();
    }

    private void updateViews(){
        counter1.setText(String.format("%d/%d", life1, poison1));
        counter2.setText(String.format("%d/%d", life2, poison2));
    }



    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt("life1",life1);
        outState.putInt("life2",life2);
        outState.putInt("poison1",poison1);
        outState.putInt("poison2",poison2);

        super.onSaveInstanceState(outState);
    }
}